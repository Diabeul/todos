# Todo project
Just a simple todos project with Java spring and angular

## Run

### Back + Front + DB
Local (Front in edit mode):
`docker-compose -f docker-compose.local.yml up`

Prod (Front with nginx):
`docker-compose -f docker-compose.prod.yml build`
`docker-compose -f docker-compose.prod.yml up`

### Front + DB
`docker-compose up`

## DB
In the docker folder in the java project (server/src/main/docker):
`docker-compose up`

## Docker registry

`docker build -f Dockerfile.prod -t registry.gitlab.com/diabeul/todos/client:latest .`
`docker push registry.gitlab.com/diabeul/todos/client:latest`