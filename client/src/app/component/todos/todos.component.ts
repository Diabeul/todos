import { Component, Input, OnInit } from '@angular/core';
import { Todo } from 'src/app/dto/todo';
import { User } from 'src/app/dto/user';
import { TodoService } from 'src/app/service/todo-service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {
  @Input() activeUser: User = null as any;
  todos: Todo[] = [];
  newTodo = '';
  editedTodo: Todo = null as any;

  visibility = 'all';
  loading = true;
  error = '';
  beforeEditCache = '';

  // Computed
  get filteredTodos(): Todo[] {
    return this.filters(this.visibility, this.todos);
  }

  get remaining(): number {
    return this.filters('active', this.todos).length;
  }

  get allDone(): boolean {
    return this.remaining === 0;
  }

  set allDone(value: boolean) {
    this.todos.forEach((todo) => (todo.completed = value));
    this.allCompleted();
  }

  get userEmail(): string {
    return this.activeUser ? this.activeUser.email : '';
  }

  get inputPlaceholder(): string {
    return this.activeUser
      ? this.activeUser.given_name + ', what needs to be done?'
      : 'What needs to be done?';
  }

  constructor(private todoService: TodoService) {}

  ngOnInit(): void {
    this.activeUser = {
      given_name: 'Yoki',
      email: 'diabeul@live.fr',
    };

    this.todoService.getAll().subscribe(
      (todos: Todo[]) => {
        console.log('Data loaded: ', todos);
        this.todos = todos;
      },
      (error) => {
        console.log(error);
        this.error = 'Failed to load todos';
      },
      () => (this.loading = false)
    );
  }

  // Methods
  filters(valididy: string, todos: Todo[]): Todo[] {
    if (valididy === 'active') {
      return todos.filter((t) => !t.completed);
    }
    if (valididy === 'completed') {
      return todos.filter((t) => t.completed);
    }
    return todos;
  }

  setVisibility(vis: string): void {
    this.visibility = vis;
  }

  addTodo(): void {
    const value = this.newTodo && this.newTodo.trim();
    if (!value) {
      return;
    }

    this.todoService
      .createNew({ id: 0, title: value, completed: false })
      .subscribe(
        (todo: Todo) => {
          console.log('New item created:', todo);
          this.todos.push({
            id: todo.id,
            title: value,
            completed: false,
          });
        },
        (error) => {
          console.log(error);
          this.error = 'Failed to add todo';
        }
      );

    this.newTodo = '';
  }

  completeTodo(todo: Todo): void {
    this.todoService.update(todo).subscribe(
      (td: Todo) => console.log('Item updated (complete):', td),
      (error) => {
        console.log(error);
        todo.completed = !todo.completed;
        this.error = 'Failed to update todo';
      }
    );
  }

  removeTodo(todo: Todo): void {
    this.todoService.remove(todo.id).subscribe(
      (td: Todo) => {
        console.log('Item removed:', td);
        this.todos.splice(this.todos.indexOf(todo), 1);
      },
      (error) => {
        console.log(error);
        this.error = 'Failed to remove todo';
      }
    );
  }

  editTodo(todo: Todo): void {
    this.beforeEditCache = todo.title;
    this.editedTodo = todo;
  }

  doneEdit(todo: Todo): void {
    if (!this.editedTodo) {
      return;
    }
    todo.title = todo.title.trim();
    this.todoService.update(todo).subscribe(
      (td: Todo) => {
        console.log('Item updated (title):', td);
        this.editedTodo = null as any;
      },
      (error) => {
        console.log(error);
        this.cancelEdit(todo);
        this.error = 'Failed to update todo';
      }
    );

    if (!todo.title) {
      this.removeTodo(todo);
    }
  }

  cancelEdit(todo: Todo): void {
    this.editedTodo = null as any;
    todo.title = this.beforeEditCache;
  }

  allCompleted(): void {
    this.todos.forEach((todo) => this.completeTodo(todo));
  }

  removeCompleted(): void {
    this.filters('completed', this.todos).forEach((todo) =>
      this.removeTodo(todo)
    );
  }

  handleErrorClick(): void {
    this.error = '';
  }

  trackByTodoId(index: number, todo: Todo): number {
    return todo.id || -1;
  }
}
