import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from 'src/app/dto/todo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from './../../environments/environment';

@Injectable({ providedIn: 'root' })
export class TodoService {
  private url = environment.apiUrl + 'todos';

  constructor(private http: HttpClient) {}

  getAll(): Observable<Todo[]> {
    return this.http
      .get<Todo[]>(this.url)
      .pipe(map((res: any) => res._embedded.todos));
  }

  createNew(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(this.url, todo);
  }

  update(todo: Todo): Observable<Todo> {
    return this.http.put<Todo>(this.url + `/${todo.id}`, todo);
  }

  remove(id: number): Observable<Todo> {
    return this.http.delete<Todo>(this.url + `/${id}`);
  }
}
