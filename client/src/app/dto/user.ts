export interface User {
  given_name: string;
  email: string;
}
