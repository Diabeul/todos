package com.yoki.todos.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Audit implements Serializable {

  @Column(name = "created_at", nullable = false, updatable = false)
  @CreationTimestamp
  @JsonIgnore
  private Date createdAt;

  @Column(name = "updated_at")
  @UpdateTimestamp
  @JsonIgnore
  private Date updatedAt;

  @Column(name = "last_updated_by", nullable = false)
  @JsonIgnore
  private String lastUpdatedBy = "rest";

  Audit(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }
}
