package com.yoki.todos.config;

import com.yoki.todos.data.Todo;
import java.util.concurrent.TimeUnit;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class RestRepositoryConfiguration implements RepositoryRestConfigurer {

  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
    config.setBasePath("api")
        .exposeIdsFor(Todo.class);

    cors.addMapping("/**")
        .allowedOriginPatterns("http://**", "https://**")
        .allowedMethods("*")
        .allowedHeaders("*")
        .allowCredentials(true)
        .maxAge(TimeUnit.DAYS.toSeconds(1));
  }

}
