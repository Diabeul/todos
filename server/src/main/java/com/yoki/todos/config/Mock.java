package com.yoki.todos.config;

import com.yoki.todos.data.Todo;
import com.yoki.todos.repo.TodoRepo;
import java.util.stream.Stream;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mock {

  @Bean
  ApplicationRunner init(TodoRepo repo) {
    return args -> Stream.of("Buy milk", "Eat pizza", "Write tutorial", "Study VueJs", "Go to BJJ")
      .map(t -> new Todo(t, "script")).forEach(repo::save);
  }
}
